<?php

declare(strict_types=1);

namespace SunnyFlail\UserUpload\Shared\Infrastructure\Utils\MessageBus;

use SunnyFlail\UserUpload\Shared\Application\Command\CommandInterface;
use SunnyFlail\UserUpload\Shared\Application\MessageBus\MessageBusInterface;
use Symfony\Component\Messenger\Exception\TransportException;
use Symfony\Component\Messenger\MessageBusInterface as SymfonyMessageBusInterface;

final readonly class MessageBus implements MessageBusInterface
{
    public function __construct(private SymfonyMessageBusInterface $messageBus)
    {
    }

    public function dispatch(CommandInterface $command): void
    {
        try {
            $this->messageBus->dispatch($command);
        } catch (TransportException $e) {
            if ($e->getPrevious()) {
                throw $e->getPrevious();
            }

            throw $e;
        }
    }
}
