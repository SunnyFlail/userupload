<?php

declare(strict_types=1);

namespace SunnyFlail\UserUpload\Shared\Infrastructure\Utils\FileUpload\Model;

use SunnyFlail\UserUpload\Shared\Application\FileUpload\Model\UriInterface;

final readonly class Uri implements UriInterface
{
    private function __construct(private string $path)
    {
    }

    public static function fromPath(string $path): static
    {
        return new Uri($path);
    }

    public function diff(UriInterface $uri): UriInterface
    {
        return Uri::fromPath(str_replace($uri->getPath(), '', $this->path));
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function __toString(): string
    {
        return $this->path;
    }
}
