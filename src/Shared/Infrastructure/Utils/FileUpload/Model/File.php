<?php

declare(strict_types=1);

namespace SunnyFlail\UserUpload\Shared\Infrastructure\Utils\FileUpload\Model;

use SunnyFlail\UserUpload\Shared\Application\FileUpload\Model\FileInterface;
use SunnyFlail\UserUpload\Shared\Application\FileUpload\Model\UriInterface;

final class File implements FileInterface
{
    public function __construct(private readonly UriInterface $uri)
    {
    }

    public function getUri(): UriInterface
    {
        return $this->uri;
    }
}
