<?php

declare(strict_types=1);

namespace SunnyFlail\UserUpload\Shared\Infrastructure\Utils\FileUpload\Model;

use SunnyFlail\UserUpload\Shared\Application\FileUpload\Model\FileInterface;
use SunnyFlail\UserUpload\Shared\Application\FileUpload\Model\UploadedFileInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile as SymfonyUploadedFile;

final readonly class UploadedFile implements UploadedFileInterface
{
    public function __construct(private SymfonyUploadedFile $file)
    {
    }

    public function getExtension(): string
    {
        return $this->file->getClientOriginalExtension();
    }

    public function move(string $directoryPath, string $fileName): FileInterface
    {
        $file = $this->file->move($directoryPath, $fileName);

        return new File(Uri::fromPath($file->getRealPath()));
    }
}
