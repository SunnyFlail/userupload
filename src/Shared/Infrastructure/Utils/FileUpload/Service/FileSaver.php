<?php

declare(strict_types=1);

namespace SunnyFlail\UserUpload\Shared\Infrastructure\Utils\FileUpload\Service;

use SunnyFlail\UserUpload\Shared\Application\FileUpload\Model\FileInterface;
use SunnyFlail\UserUpload\Shared\Application\FileUpload\Model\UploadedFileInterface;
use SunnyFlail\UserUpload\Shared\Application\FileUpload\Service\FileNameGeneratorInterface;
use SunnyFlail\UserUpload\Shared\Application\FileUpload\Service\FileSaverInterface;

final readonly class FileSaver implements FileSaverInterface
{
    public function __construct(
        private FileNameGeneratorInterface $nameGenerator,
        private string $uploadDirectory,
    ) {
    }

    public function saveFile(UploadedFileInterface $file): FileInterface
    {
        return $file->move(
            $this->uploadDirectory,
            $this->nameGenerator->generateName($file)
        );
    }
}
