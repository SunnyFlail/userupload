<?php

declare(strict_types=1);

namespace SunnyFlail\UserUpload\Shared\Infrastructure\Utils\FileUpload\Service;

use SunnyFlail\UserUpload\Shared\Application\FileUpload\Model\UploadedFileInterface;
use SunnyFlail\UserUpload\Shared\Application\FileUpload\Service\FileNameGeneratorInterface;

final readonly class FileNameGenerator implements FileNameGeneratorInterface
{
    public function generateName(UploadedFileInterface $file): string
    {
        return sprintf('%s.%s', uniqid(), $file->getExtension());
    }
}
