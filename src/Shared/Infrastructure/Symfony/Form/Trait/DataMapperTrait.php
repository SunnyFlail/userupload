<?php

declare(strict_types=1);

namespace SunnyFlail\UserUpload\Shared\Infrastructure\Symfony\Form\Trait;

use SunnyFlail\UserUpload\Shared\Infrastructure\Symfony\Form\Result\ResultInterface;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Form\FormInterface;

/**
 * @template-covariant Result of ResultInterface
 */
trait DataMapperTrait
{
    final public function mapFormsToData(\Traversable $forms, mixed &$viewData): void
    {
        $viewData = $this->populateResult(iterator_to_array($forms));
    }

    final public function mapDataToForms(mixed $viewData, \Traversable $forms): void
    {
        $resultClass = self::getResultClass();

        if (!$viewData || !$resultClass) {
            return;
        }

        if ($viewData::class !== $resultClass) {
            throw new UnexpectedTypeException($viewData, $resultClass);
        }

        $this->populateForm(iterator_to_array($forms), $viewData);
    }

    /**
     * @return class-string<Result>|null
     */
    protected static function getResultClass(): ?string
    {
        return null;
    }

    /**
     * @param array<string,FormInterface>
     *
     * @return Result
     */
    abstract protected function populateResult(array $forms): ResultInterface;

    /**
     * @param array<string,FormInterface> $forms
     * @param Result                      $result
     */
    protected function populateForm(array $forms, ResultInterface $result): void
    {
    }
}
