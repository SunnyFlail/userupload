<?php

namespace SunnyFlail\UserUpload\Shared\Infrastructure\Symfony;

use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Component\HttpKernel\Kernel as BaseKernel;
use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;

class Kernel extends BaseKernel
{
    use MicroKernelTrait;

    private function configureRoutes(RoutingConfigurator $routes): void
    {
        $codeDir = $this->getProjectDir().'/src';

        $routes->import($codeDir.'/*/Infrastructure/Symfony/Resources/config/routes.xml');
    }
}
