<?php

declare(strict_types=1);

namespace SunnyFlail\UserUpload\Shared\Infrastructure\Symfony\Trait;

use Symfony\Component\Form\FormInterface;

trait GetFormErrorsTrait
{
    protected function getFormErrors(FormInterface $form): array
    {
        $errors = [];

        /* @var FormError $err */
        foreach ($form->getErrors(true) as $error) {
            $errors[] = $error->getMessage();
        }

        return $errors;
    }
}
