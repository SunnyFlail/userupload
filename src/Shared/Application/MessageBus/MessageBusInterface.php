<?php

declare(strict_types=1);

namespace SunnyFlail\UserUpload\Shared\Application\MessageBus;

use SunnyFlail\UserUpload\Shared\Application\Command\CommandInterface;

interface MessageBusInterface
{
    public function dispatch(CommandInterface $command): void;
}
