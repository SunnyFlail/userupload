<?php

declare(strict_types=1);

namespace SunnyFlail\UserUpload\Shared\Application\FileUpload\Model;

interface UriInterface extends \Stringable
{
    /**
     * Calculates and returns difference in URI paths.
     */
    public function diff(UriInterface $uri): UriInterface;

    public function getPath(): string;
}
