<?php

declare(strict_types=1);

namespace SunnyFlail\UserUpload\Shared\Application\FileUpload\Model;

interface UploadedFileInterface
{
    public function getExtension(): string;

    public function move(string $pathToFile, string $fileName): FileInterface;
}
