<?php

declare(strict_types=1);

namespace SunnyFlail\UserUpload\Shared\Application\FileUpload\Model;

interface FileInterface
{
    public function getUri(): UriInterface;
}
