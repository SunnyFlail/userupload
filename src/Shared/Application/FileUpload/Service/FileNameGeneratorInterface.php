<?php

declare(strict_types=1);

namespace SunnyFlail\UserUpload\Shared\Application\FileUpload\Service;

use SunnyFlail\UserUpload\Shared\Application\FileUpload\Model\UploadedFileInterface;

interface FileNameGeneratorInterface
{
    public function generateName(UploadedFileInterface $file): string;
}
