<?php

declare(strict_types=1);

namespace SunnyFlail\UserUpload\Shared\Application\FileUpload\Service;

use SunnyFlail\UserUpload\Shared\Application\FileUpload\Model\FileInterface;
use SunnyFlail\UserUpload\Shared\Application\FileUpload\Model\UploadedFileInterface;

interface FileSaverInterface
{
    public function saveFile(UploadedFileInterface $file): FileInterface;
}
