<?php

declare(strict_types=1);

namespace SunnyFlail\UserUpload\Shared\Application\Handler;

use SunnyFlail\UserUpload\Shared\Application\Command\CommandInterface;

/**
 * @template T of CommandInterface
 */
interface MessageHandlerInterface
{
}
