<?php

declare(strict_types=1);

namespace SunnyFlail\UserUpload\UserData\Infrastructure\Symfony\Controller;

use SunnyFlail\UserUpload\UserData\Domain\Repository\UserDataRepositoryInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

final class UserDataListingController extends AbstractController
{
    public function __construct(private readonly UserDataRepositoryInterface $repository)
    {
    }

    public function index(): Response
    {
        return $this->render(
            'userData/listing.twig', [
                'collection' => $this->repository->findForListing(),
            ]
        );
    }
}
