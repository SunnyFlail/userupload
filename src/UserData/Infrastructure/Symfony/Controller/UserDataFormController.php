<?php

declare(strict_types=1);

namespace SunnyFlail\UserUpload\UserData\Infrastructure\Symfony\Controller;

use SunnyFlail\UserUpload\Shared\Application\FileUpload\Service\FileSaverInterface;
use SunnyFlail\UserUpload\Shared\Application\MessageBus\MessageBusInterface;
use SunnyFlail\UserUpload\Shared\Infrastructure\Symfony\Trait\GetFormErrorsTrait;
use SunnyFlail\UserUpload\Shared\Infrastructure\Utils\FileUpload\Model\UploadedFile;
use SunnyFlail\UserUpload\UserData\Application\Command\AddUserData;
use SunnyFlail\UserUpload\UserData\Infrastructure\Symfony\Form\AddUserDataForm;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class UserDataFormController extends AbstractController
{
    use GetFormErrorsTrait;

    public function __construct(
        private readonly MessageBusInterface $messageBus,
        private FileSaverInterface $fileSaver
    ) {
    }

    public function index(): Response
    {
        return $this->render(
            'userData/form.twig', [
                'form' => $this->createForm(AddUserDataForm::class),
            ]
        );
    }

    public function save(Request $request): JsonResponse
    {
        $form = $this->createForm(AddUserDataForm::class);
        $form->handleRequest($request);

        if (!$form->isValid()) {
            return $this->json([
                    'message' => "Provided data doesn't pass validation",
                    'errors' => $this->getFormErrors($form),
                ], Response::HTTP_BAD_REQUEST
            );
        }

        try {
            $result = $form->getData();
            $this->messageBus->dispatch(
                new AddUserData(
                    $result->firstName,
                    $result->secondName,
                    $this->fileSaver->saveFile(new UploadedFile($result->attachment))
                )
            );
        } catch (\Throwable $e) {
            // dd($e);
            return $this->json(
                ['message' => 'An error occured while processing your request'],
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }

        return $this->json(['message' => 'Successfully added new data']);
    }
}
