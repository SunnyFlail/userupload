<?php

declare(strict_types=1);

namespace SunnyFlail\UserUpload\UserData\Infrastructure\Symfony\Form;

use SunnyFlail\UserUpload\Shared\Infrastructure\Symfony\Form\Result\ResultInterface;
use SunnyFlail\UserUpload\Shared\Infrastructure\Symfony\Form\Trait\DataMapperTrait;
use SunnyFlail\UserUpload\UserData\Infrastructure\Symfony\Form\Result\AddUserDataResult;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\DataMapperInterface;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints;

final class AddUserDataForm extends AbstractType implements DataMapperInterface
{
    /**
     * @template-use DataMapperTrait<AddUserDataResult>
     */
    use DataMapperTrait;

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('firstName', TextType::class, [
                'trim' => true,
                'constraints' => [new Constraints\NotBlank()],
            ])
            ->add('secondName', TextType::class, [
                'trim' => true,
                'constraints' => [new Constraints\NotBlank()],
            ])
            ->add('attachment', FileType::class, [
                'constraints' => [new Constraints\Image(maxSize: '2M')],
            ])
            ->add('save', SubmitType::class)
            ->setDataMapper($this)
        ;
    }

    protected function populateResult(array $forms): ResultInterface
    {
        return new AddUserDataResult(
            $forms['firstName']->getData(),
            $forms['secondName']->getData(),
            $forms['attachment']->getData(),
        );
    }
}
