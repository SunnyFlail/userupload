<?php

declare(strict_types=1);

namespace SunnyFlail\UserUpload\UserData\Infrastructure\Symfony\Form\Result;

use SunnyFlail\UserUpload\Shared\Infrastructure\Symfony\Form\Result\ResultInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

final readonly class AddUserDataResult implements ResultInterface
{
    public function __construct(
        public string $firstName,
        public string $secondName,
        public UploadedFile $attachment
    ) {
    }
}
