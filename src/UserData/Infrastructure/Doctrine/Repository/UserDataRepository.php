<?php

declare(strict_types=1);

namespace SunnyFlail\UserUpload\UserData\Infrastructure\Doctrine\Repository;

use Doctrine\ORM\EntityManagerInterface;
use SunnyFlail\UserUpload\UserData\Domain\Model\UserDataInterface;
use SunnyFlail\UserUpload\UserData\Domain\Repository\UserDataRepositoryInterface;
use SunnyFlail\UserUpload\UserData\Infrastructure\Doctrine\Entity\UserData;

final readonly class UserDataRepository implements UserDataRepositoryInterface
{
    public function __construct(private EntityManagerInterface $entityManager)
    {
    }

    public function save(UserDataInterface $userData): void
    {
        $this->entityManager->persist($userData);
        $this->entityManager->flush();
    }

    public function findForListing(): iterable
    {
        return $this->entityManager->createQueryBuilder()
            ->select('ud')
            ->from(UserData::class, 'ud')
            ->getQuery()
            ->execute()
        ;
    }
}
