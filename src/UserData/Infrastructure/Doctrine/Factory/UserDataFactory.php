<?php

declare(strict_types=1);

namespace SunnyFlail\UserUpload\UserData\Infrastructure\Doctrine\Factory;

use SunnyFlail\UserUpload\Shared\Application\FileUpload\Model\FileInterface;
use SunnyFlail\UserUpload\Shared\Application\FileUpload\Model\UriInterface;
use SunnyFlail\UserUpload\Shared\Infrastructure\Utils\FileUpload\Model\Uri;
use SunnyFlail\UserUpload\UserData\Application\Factory\UserDataFactoryInterface;
use SunnyFlail\UserUpload\UserData\Domain\Model\UserDataInterface;
use SunnyFlail\UserUpload\UserData\Infrastructure\Doctrine\Entity\UserData;

final readonly class UserDataFactory implements UserDataFactoryInterface
{
    private UriInterface $uri;

    public function __construct(string $publicDirectory)
    {
        $this->uri = Uri::fromPath($publicDirectory);
    }

    public function create(string $firstName, string $lastName, FileInterface $file): UserDataInterface
    {
        return new UserData(
            $firstName,
            $lastName,
            $file->getUri()->diff($this->uri)->__toString()
        );
    }
}
