<?php

declare(strict_types=1);

namespace SunnyFlail\UserUpload\UserData\Infrastructure\Doctrine\Entity;

use Doctrine\ORM\Mapping as ORM;
use SunnyFlail\UserUpload\UserData\Domain\Model\UserDataInterface;
use SunnyFlail\UserUpload\UserData\Infrastructure\Doctrine\Repository\UserDataRepository;

#[ORM\Entity(UserDataRepository::class)]
final class UserData implements UserDataInterface
{
    #[ORM\Id()]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private int $id;

    #[ORM\Column]
    private string $firstName;

    #[ORM\Column]
    private string $lastName;

    #[ORM\Column]
    private string $attachmentPath;

    public function __construct(
        string $firstName,
        string $lastName,
        string $attachmentPath
    ) {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->attachmentPath = $attachmentPath;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function getAttachmentPath(): string
    {
        return $this->attachmentPath;
    }
}
