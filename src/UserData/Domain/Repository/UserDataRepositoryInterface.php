<?php

declare(strict_types=1);

namespace SunnyFlail\UserUpload\UserData\Domain\Repository;

use SunnyFlail\UserUpload\UserData\Domain\Model\UserDataInterface;

interface UserDataRepositoryInterface
{
    /**
     * @return iterable<UserDataInterface>
     */
    public function findForListing(): iterable;

    public function save(UserDataInterface $userData): void;
}
