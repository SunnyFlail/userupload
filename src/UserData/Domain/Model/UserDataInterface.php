<?php

declare(strict_types=1);

namespace SunnyFlail\UserUpload\UserData\Domain\Model;

interface UserDataInterface
{
    public function getFirstName(): string;

    public function getLastName(): string;

    public function getAttachmentPath(): string;
}
