<?php

declare(strict_types=1);

namespace SunnyFlail\UserUpload\UserData\Application\Handler;

use SunnyFlail\UserUpload\Shared\Application\Handler\MessageHandlerInterface;
use SunnyFlail\UserUpload\UserData\Application\Command\AddUserData;
use SunnyFlail\UserUpload\UserData\Application\Factory\UserDataFactoryInterface;
use SunnyFlail\UserUpload\UserData\Domain\Repository\UserDataRepositoryInterface;

/**
 * @template-implements MessageHandlerInterface<AddUserData>
 */
final readonly class AddUserDataHandler implements MessageHandlerInterface
{
    public function __construct(
        private UserDataRepositoryInterface $repository,
        private UserDataFactoryInterface $factory
    ) {
    }

    public function __invoke(AddUserData $command): void
    {
        $this->repository->save($this->factory->create(
            $command->firstName,
            $command->lastName,
            $command->attachment
        ));
    }
}
