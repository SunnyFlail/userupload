<?php

declare(strict_types=1);

namespace SunnyFlail\UserUpload\UserData\Application\Factory;

use SunnyFlail\UserUpload\Shared\Application\FileUpload\Model\FileInterface;
use SunnyFlail\UserUpload\UserData\Domain\Model\UserDataInterface;

interface UserDataFactoryInterface
{
    public function create(
        string $firstName,
        string $lastName,
        FileInterface $file
    ): UserDataInterface;
}
