<?php

declare(strict_types=1);

namespace SunnyFlail\UserUpload\UserData\Application\Command;

use SunnyFlail\UserUpload\Shared\Application\Command\CommandInterface;
use SunnyFlail\UserUpload\Shared\Application\FileUpload\Model\FileInterface;

final readonly class AddUserData implements CommandInterface
{
    public function __construct(
        public string $firstName,
        public string $lastName,
        public FileInterface $attachment
    ) {
    }
}
