# User Upload
## Instalation

Make sure you have docker daemon up and running.
Having this directory active, in your favourite terminal, run command:
```bash
docker compose up -d
```

After docker pulls and builds needed containers, you should log into container with php, and run migration
```bash
docker compose exec server bin/console doctrine:migrations:migrate
```

Now you are ready to open the site in your browser, it should be available under `localhost` domain

## User Form
The upload form is available under
`/user_data/form`

## Data listing
The listing is available under
`/user_data/listing`