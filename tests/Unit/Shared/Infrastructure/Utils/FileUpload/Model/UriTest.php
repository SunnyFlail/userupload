<?php

declare(strict_types=1);

namespace SunnyFlail\UserUpload\Test\Shared\Infrastructure\Utils\FileUpload\Model;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\TestCase;
use SunnyFlail\UserUpload\Shared\Infrastructure\Utils\FileUpload\Model\Uri;

#[CoversClass(Uri::class)]
final class UriTest extends TestCase
{
    public function testDiff(): void
    {
        $replacedPath = Uri::fromPath('/var/www/public');
        $SUT = Uri::fromPath('/var/www/public/uploaded/');

        $resultUri = $SUT->diff($replacedPath);

        $this->assertSame('/uploaded/', $resultUri->getPath());
    }
}
