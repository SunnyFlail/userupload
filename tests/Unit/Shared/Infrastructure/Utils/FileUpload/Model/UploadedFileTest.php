<?php

declare(strict_types=1);

namespace SunnyFlail\UserUpload\Test\Shared\Infrastructure\Utils\FileUpload\Model;

use PHPUnit\Framework\TestCase;
use SunnyFlail\UserUpload\Shared\Infrastructure\Utils\FileUpload\Model\UploadedFile;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile as SymfonyUploadedFile;

final class UploadedFileTest extends TestCase
{
    public function testMove(): void
    {
        $directoryPath = '/var/www/public/uploaded/';
        $fileName = 'test.png';
        $expectedPath = $directoryPath . $fileName;
        $file = $this->createMock(File::class);
        $uploadedFile = $this->createMock(SymfonyUploadedFile::class);
        
        $SUT = new UploadedFile($uploadedFile);

        $uploadedFile->expects($this->once())
            ->method('move')
            ->with($directoryPath, $fileName)
            ->willReturn($file)
        ;
        $file->expects($this->once())
            ->method('getRealPath')
            ->willReturn($expectedPath)
        ;

        $result = $SUT->move($directoryPath, $fileName);

        $this->assertSame($expectedPath, $result->getUri()->getPath());
    }
}
